# This module works with Python 3
%if 0%{?fedora} || 0%{?rhel} >= 7
%global with_python3 1
%endif

Name:           python-shapely
Version:        1.5.2
Release:        2.2%{?dist}
Summary:        Manipulation and analysis of geometric objects in the Cartesian plane

Group:          Development/Libraries
License:        BSD
URL:            http://toblerity.org/shapely
Source0:        http://pypi.python.org/packages/source/S/Shapely/Shapely-%{version}.tar.gz
BuildRequires:  python2-devel
BuildRequires:  Cython
BuildRequires:  python-setuptools
BuildRequires:  geos-devel
# Tests use numpy
BuildRequires:  numpy

%if 0%{?with_python3}
BuildRequires:  python3-devel
BuildRequires:  python36-Cython
BuildRequires:  python3-setuptools
BuildRequires:  python36-numpy
BuildRequires:  python36-pytest
%endif # if with_python3


# We don't want to provide private python extension libs
%{?filter_setup:
%filter_provides_in %{python_sitearch}/.*\.so$ 
%filter_setup
}

%description
Shapely is a package for creation, manipulation, and analysis
of planar geometry objects – designed especially for developers
of cutting edge geographic information systems. In a nutshell:
Shapely lets you do PostGIS-ish stuff outside the context of a
database using idiomatic Python.

You can use this package with python-matplotlib and numpy.
See README.rst for more information!

%if 0%{?with_python3}
%package -n python%{python3_pkgversion}-shapely
%{?python_provide:%python_provide python%{python3_pkgversion}-shapely}
Summary:        Manipulation and analysis of geometric objects in the Cartesian plane
Group:          Development/Libraries

%description -n python%{python3_pkgversion}-shapely
Shapely is a package for creation, manipulation, and analysis
of planar geometry objects – designed especially for developers
of cutting edge geographic information systems. In a nutshell:
Shapely lets you do PostGIS-ish stuff outside the context of a
database using idiomatic Python.

You can use this package with python3-matplotlib and python3-numpy.
See README.rst for more information!

%endif # if with_python3


%prep
%setup -q -n Shapely-%{version}

%if 0%{?with_python3}
rm -rf %{py3dir}
cp -a . %{py3dir}
find %{py3dir} -name '*.py' | xargs sed -i '1s|^#!python|#!%{__python3}|'
%endif # if with_python3


%build
%{__python} setup.py build

%if 0%{?with_python3}
pushd %{py3dir}
%{__python3} setup.py build
%endif # if with_python3


%check
%{__python} setup.py test

%if 0%{?with_python3}
# python3 tests fail due to missing pylab extension
#pushd %{py3dir}
#%{__python3} setup.py test
%endif # if with_python3


%install
%{__python} setup.py install --skip-build --root %{buildroot}

%if 0%{?with_python3}
pushd %{py3dir}
%{__python3} setup.py install --skip-build --root %{buildroot}
%endif # if with_python3

rm -f %{buildroot}%{_prefix}/shapely/_geos.pxi

%files
%doc CHANGES.txt README.rst CREDITS.txt LICENSE.txt
%doc docs
%doc %{python_sitearch}/shapely/examples/
%{python_sitearch}/shapely/algorithms/
%{python_sitearch}/shapely/geometry/
%{python_sitearch}/shapely/speedups/
%{python_sitearch}/shapely/vectorized/
%{python_sitearch}/shapely/*.py*
%{python_sitearch}/Shapely-%{version}-py*.egg-info

%if 0%{?with_python3}
%files -n python%{python3_pkgversion}-shapely
%doc CHANGES.txt README.rst CREDITS.txt LICENSE.txt
%doc docs
%doc %{python3_sitearch}/shapely/examples
%{python3_sitearch}/shapely/algorithms/
%{python3_sitearch}/shapely/geometry/
%{python3_sitearch}/shapely/speedups/
%{python3_sitearch}/shapely/vectorized/
%{python3_sitearch}/shapely/*.py*
%{python3_sitearch}/Shapely-%{version}-py*.egg-info
%{python3_sitearch}/shapely/__pycache__/*
%endif # with_python3

%changelog
* Fri Nov 22 2019 Michael Thomas <michael.thomas@LIGO.ORG> - 1.5.2-2.2
- Clean up Provides

* Thu Nov 14 2019 Michael Thomas <michael.thomas@LIGO.ORG> - 1.5.2-2.1
- Build for python3.6 on EPEL

* Thu Jun 18 2015 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.5.2-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_23_Mass_Rebuild

* Mon Jan  5 2015 Volker Fröhlich <volker27@gmx.at> - 1.5.2-1
- New upstream release

* Sat Dec  6 2014 Volker Fröhlich <volker27@gmx.at> - 1.5.1-1
- New upstream release

* Tue Nov  4 2014 Volker Fröhlich <volker27@gmx.at> - 1.4.4-1
- New upstream release

* Wed Oct  8 2014 Volker Fröhlich <volker27@gmx.at> - 1.4.3-1
- New upstream release

* Tue Sep 30 2014 Volker Fröhlich <volker27@gmx.at> - 1.4.2-1
- New upstream release

* Wed Sep 24 2014 Volker Fröhlich <volker27@gmx.at> - 1.4.1-1
- New upstream release

* Thu Sep 18 2014 Volker Fröhlich <volker27@gmx.at> - 1.4.0-1
- New upstream release
- Add BR on Cython/python3-cython and build the C extension
- Update URL
- Remove the obsolete encoding patch

* Sun Aug 17 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.3.2-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_22_Mass_Rebuild

* Sat Jun 07 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.3.2-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_Mass_Rebuild

* Wed May 28 2014 Kalev Lember <kalevlember@gmail.com> - 1.3.2-2
- Rebuilt for https://fedoraproject.org/wiki/Changes/Python_3.4

* Fri May 23 2014 Volker Fröhlich <volker27@gmx.at> - 1.3.2-1
- New upstream release

* Thu Apr 24 2014 Volker Fröhlich <volker27@gmx.at> - 1.3.1-1
- New upstream release

* Sat Apr 19 2014 Volker Fröhlich <volker27@gmx.at> - 1.3.0-2
- Replace obsolete python-setuptools-devel with python-setuptools

* Wed Feb 12 2014 Volker Fröhlich <volker27@gmx.at> - 1.3.0-1
- New upstream release
- Use a better summary
- Add Python 3 builds
- Change BR python-devel to python2-devel

* Mon Sep 16 2013 Volker Fröhlich <volker27@gmx.at> - 1.2.18-1
- New upstream release

* Sun Aug 04 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.2.17-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_20_Mass_Rebuild

* Sun Jan 27 2013 Volker Fröhlich <volker27@gmx.at> - 1.2.17-1
- New upstream release

* Tue Sep 18 2012 Volker Fröhlich <volker27@gmx.at> - 1.2.16-1
- New upstream release

* Sat Jul 21 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.2.15-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_18_Mass_Rebuild

* Thu Jun 28 2012 Volker Fröhlich <volker27@gmx.at> - 1.2.15-1
- New upstream release
- Pyx file is no longer part of the sources, thus Cython is no longer BR
- Chaintest is working fine now, drop the patch

* Sun Apr  8 2012 Volker Fröhlich <volker27@gmx.at> - 1.2.14-1
- Update for release 1.2.14
- Remove duplicate PKG-INFO file
- Correct description -- pointing to README.rst now
- Add patch that corrects the attribute chaining test
- Tests now work fine, therefore respect their outcome
- Remove ready-made _speedups.c

* Sat Jan 14 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.2.13-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_17_Mass_Rebuild

* Tue Aug 16 2011 Volker Fröhlich <volker27@gmx.at> - 1.2.13-1
- Update for release 1.2.13

* Tue Aug 16 2011 Volker Fröhlich <volker27@gmx.at> - 1.2.12-1
- Update for release 1.2.12
- Don't ship tests
- Label examples as documentation

* Mon Aug 15 2011 Volker Fröhlich <volker27@gmx.at> - 1.2.11-2
- BR numpy for the tests

* Mon Aug 15 2011 Ville Skyttä <ville.skytta@iki.fi> - 1.2.11-2
- BR geos-devel to actually build arch specific bits
- Drop unneeded geos dep

* Fri Aug 12 2011 Volker Fröhlich <volker27@gmx.at> - 1.2.11-1 
- Updated for 1.2.11
- Switch away from noarch
- Remove useless clean section and rm in install
- Debian patch to rebuild Cython .c file
- Avoid private provides for .so
- Extend package description

* Fri Apr 01 2011 Volker Fröhlich <volker27@gmx.at> - 1.2.9-1 
- Updated for 1.2.9
- Added tests again, but ignore the results

* Fri Feb 25 2011 Volker Fröhlich <volker27@gmx.at> - 1.2.8-1 
- Updated for 1.2.8
- Disable tests

* Wed Feb 09 2011 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.2.7-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_15_Mass_Rebuild

* Sat Nov 27 2010 Volker Fröhlich <volker27@gmx.at> - 1.2.7-2 
- Explained excluded files; added check section

* Wed Nov 24 2010 Volker Fröhlich <volker27@gmx.at> - 1.2.7-1 
- Initial package for Fedora
